﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : MonoBehaviour
{
	[SerializeField] private float _speed;
	[SerializeField] private float _fearTime = 5f;
	
	public Book _book;
	public NavMeshAgent NavAgent { get; private set; }

	private void Awake()
	{
		NavAgent = GetComponent<NavMeshAgent>();
		NavAgent.speed = _speed;
	}

	public void ChangeTarget(Vector3 target)
	{
		NavAgent.SetDestination(target);
	}

	public void FollowBook()
	{
		NavAgent.SetDestination(_book.transform.position);
	}
	
	public void RunAway(Vector3 target)
	{
		CancelInvoke("FollowBook");
		Vector3 direcaoFuga = (transform.position - target).normalized;
		Vector3 novoDestino = transform.position + direcaoFuga * 25f;
		novoDestino.y = 0;
		
		NavAgent.SetDestination(novoDestino);
		Invoke("FollowBook", _fearTime);
	}

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Cauldron"))
		{
			Destroy(gameObject);
		}else if (other.CompareTag("Book"))
		{
			other.GetComponent<Book>().AddGhost();
			Destroy(gameObject);
		}
	}
}