﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Book : MonoBehaviour
{
	[SerializeField] private Slider _ghostCounterSlider;
	[SerializeField] private Text _gameOverTxt;
	public int _ghostCounter { get; private set; }

	private void Awake()
	{
		_ghostCounter = 0;
	}

	private void Update () 
	{
		if (_ghostCounter == -1)
			return;
		
		if (_ghostCounter == 20)
		{
			GetComponent<Animator>().SetTrigger("GameOver");
			_gameOverTxt.text = "Game Over";
			_gameOverTxt.gameObject.SetActive(true);
			
			_ghostCounter = -1;
		}
	}

	public void AddGhost()
	{
		if (_ghostCounter == -1)
			return;
		
		_ghostCounter++;
		_ghostCounterSlider.value += 1f / 20f;
	}
}