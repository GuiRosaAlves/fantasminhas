﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class _SpawnManager : MonoBehaviour
{
	[SerializeField] private Book _book;
	[SerializeField] private float _timeLimit = 20f;
	[SerializeField] private Ghost _ghostPrefab;
	[SerializeField] private Transform[] _spawnPoint;
	[SerializeField] private float _spawnRate;
	private float _timer = 0f;
	private float _endGameTimer = 0f;
	[SerializeField] private Text _currTimeTxt;
	[SerializeField] private Text _gameOverTxt;
	
	private void Start()
	{
		_endGameTimer = _timeLimit;
		SpawnGhost();
	}

	private void Update ()
	{
		if (_book._ghostCounter == -1)
			return;
		
		if (_endGameTimer > 0f)
		{
			_timer += Time.deltaTime;
			_endGameTimer -= Time.deltaTime;
		
			if (_timer >= _spawnRate)
			{
				_timer = 0f;
				SpawnGhost();
			}

			_endGameTimer = (_endGameTimer <= 0f) ? 0f : _endGameTimer;
			_currTimeTxt.text = _endGameTimer.ToString("F2");
		}
		else
		{
			_gameOverTxt.text = "You Won!!";
			_gameOverTxt.gameObject.SetActive(true);
		}
	}

	public void SpawnGhost()
	{
		var ghost = Instantiate(_ghostPrefab, _spawnPoint[Random.Range(0, _spawnPoint.Length)].position, Quaternion.identity);
		ghost._book = this._book;
		ghost.FollowBook();
	}
}