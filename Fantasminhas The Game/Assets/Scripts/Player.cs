﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

public class Player : MonoBehaviour
{
	[SerializeField] private float _fearAreaofEffect = 2.5f;
	[SerializeField] private GameObject _clickSprite;
	
	void Update ()
	{
		var hit = new RaycastHit();
		var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray, out hit, 1000, LayerMask.GetMask("Ground")))
		{
			if (Input.GetMouseButtonDown((int)MouseButton.LeftMouse))
			{
				var raycastHits = Physics.SphereCastAll(hit.point, _fearAreaofEffect, Vector3.right, Mathf.Infinity, LayerMask.GetMask("Ghost"));
				for (int i = 0; i < raycastHits.Length; i++)
				{
					raycastHits[i].transform.GetComponent<Ghost>().RunAway(hit.point);
				}
				_clickSprite.transform.position = new Vector3(hit.point.x, _clickSprite.transform.position.y, hit.point.z);
				_clickSprite.transform.GetComponent<Animator>().SetTrigger("Blink");
			}
		}
	}
}
